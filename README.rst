DRM Maintainer Tools
====================

This project covers the tools and documentation for maintaining, committing, and
contributing to the Linux kernel DRM subsystem's drm-misc and drm-intel
repositories. The intended audience is primarily the maintainers and committers
of said repositories, but the workflow documentation may be useful for anyone
interested in the kernel graphics subsystem development.

Please see the `DRM Maintainer Tools Documentation`_ for more information, and
`contributing and contact`_ on how to collaborate on the documentation and
tools.

.. _DRM Maintainer Tools Documentation: https://drm.pages.freedesktop.org/maintainer-tools/

.. _contributing and contact: https://drm.pages.freedesktop.org/maintainer-tools/CONTRIBUTING.html
